// We just need to require the directory because it will automatically add `index.js`
var db = require('../models');


exports.getTodos = function(req, res) {
  db.Todo.find()
  .then(function(todos) {
    res.json(todos);
  })
  .catch(function(err) {
    res.send(err);
  });
}

exports.createTodo = function(req, res) {
  db.Todo.create(req.body)
  .then(function(newTodo) {
    // `201` status means "created"
    res.status(201).json(newTodo);
  })
  .catch(function(err) {
    res.send(err);
  });
}

exports.getTodo = function(req, res) {
  db.Todo.findById(req.params.todoId)
  .then(function(foundTodo) {
    res.json(foundTodo);
  })
  .catch(function(err) {
    res.send(err);
  });
}

exports.updateTodo = function(req, res) {
  // Find one by the `_id` given in `todoId` and update with the data given in the body
  //  `{new: true}` shows the `updatedTodo` AFTER being updated instead of it's value BEFORE
  db.Todo.findOneAndUpdate({_id: req.params.todoId}, req.body, {new: true})
  .then(function(updatedTodo) {
    res.json(updatedTodo);
  })
  .catch(function(err) {
    res.send(err);
  });
}

exports.deleteTodo = function(req, res) {
  db.Todo.remove({_id: req.params.todoId})
  .then(function(deletedTodo) {
    res.json({message: 'Successfully deleted todo from DB.'});
  })
  .catch(function(err) {
    res.send(err);
  });
}


module.exports = exports;

