var mongoose = require('mongoose');
// Allows you to see logs when interacting with the DB
mongoose.set('debug', true);
// Connect to `todo-api` on local mongoDB 
mongoose.connect('mongodb://localhost/todo-api');

// Allows you to use Promise syntax. 
mongoose.Promise = Promise;

module.exports.Todo = require("./todos");