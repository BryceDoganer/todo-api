var mongoose = require("mongoose");

var todoSchema = new mongoose.Schema({
  name: {
    type: String,
    required: 'Name cannot be blank!'
  },
  completed: {
    type: Boolean,
    default: false 
  },
  created_date: {
    type: Date,
    default: Date.now
  }
});

// Save our schema to a mongoose model in a `Todos` collection 
var Todo = mongoose.model('Todo', todoSchema);
// Anytime this file is required they are receiving this Todo model.
module.exports = Todo;