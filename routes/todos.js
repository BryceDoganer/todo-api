var express = require('express');
// Allows you to break up routes into separate files 
var router = express.Router();
// We just need to require the directory because it will automatically add `index.js`
var db = require('../models');
var helpers = require('../helpers/todos')

// In our main `index.js` file, `/api/todos` is added before all routes in this file.
// INDEX & Create Route
router.route('/')
  .get(helpers.getTodos)
  .post(helpers.createTodo);

// SHOW, UPDATE & DESTORY RouteS
router.route('/:todoId')
  .get(helpers.getTodo)
  .put(helpers.updateTodo)
  .delete(helpers.deleteTodo);
  
module.exports = router;