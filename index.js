var express    = require("express"),
    app        = express(),
    port       = process.env.PORT || 3000,
    ip         = process.env.IP,
    bodyParser = require('body-parser');
    
var todoRoutes = require('./routes/todos'); 

// Setup bodyParser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));
// Setup express to use the files in the `views` directory
app.use(express.static(__dirname + '/views'));
app.use(express.static(__dirname + '/public'));

app.get("/", function(req, res) {
  res.sendFile("index.html");
});

// Concatenates `/api/todos` before all routes in `todoRoutes`
app.use('/api/todos', todoRoutes);
    
app.listen(port, ip, function() {
  console.log("App started on port " + port);
});