/* global $ */ 
$(document).ready(function() {
  $.getJSON("/api/todos")
  .then(addTodos) 
  .catch(function(err) {
    console.log(err);
  });
  
  $('#todoInput').keypress(function(event) {
    if(event.which == 13) {
      createTodo();
    }
  });
  
  $('.list').on('click', '.task', function() {
    updateTodo($(this));
  });
  
  // ** Important for single page apps **
  // Because the spans don't exist when the page loads we have to attach this listener 
  //  to the `.list` and have it only execute when it's a span inside of the .list.
  $('.list').on('click', 'span', function(event) {
    // `stopPropagation()` keeps the event from bubbling up to parent listeners  
    event.stopPropagation();
    removeTodo($(this).parent());
  });
});

function todoUrl(todo) {
  var id = todo.data('_id');
  return '/api/todos/' + id;
} 

function addTodos(todos) {
  // Add todos to page here 
  todos.forEach(function(todo) {
    addTodo(todo);
  });
}

function addTodo(todo) {
  var newTodo = $('<li class="task">'+ todo.name + '<span>X</span></li>');
  // You can add data to an element through jQuery. We need the `_id` so we can
  //  delete the todo from the DB
  newTodo.data('_id', todo._id);
  newTodo.data('completed', todo.completed);
  if(todo.completed) {
    newTodo.addClass("done");
  }
  $('.list').append(newTodo);
}

function removeTodo(todo) {
  $.ajax({
    method: 'DELETE',
    url: todoUrl(todo)
  })
  .then(function(data) {
    todo.remove();
    console.log(data);
  })
  .catch(function(err) {
    console.log(err);
  });
}

function updateTodo(todo) {
  var isDone = !todo.data('completed');
  var updateData = {completed: isDone};
  $.ajax({
    method: 'PUT',
    url: todoUrl(todo),
    data: updateData
  })
  .then(function(data) {
    todo.toggleClass('done');
    todo.data('completed', isDone);
    console.log(data);
  })
  .catch(function(err) {
    console.log(err);
  });
}

function createTodo() {
  // Send request to create new todo 
  var userInput = $('#todoInput').val();
  $.post('/api/todos', {name: userInput})
  .then(function(newTodo) {
    if(newTodo.name != "ValidationError") {
     $('#todoInput').val("");
     addTodo(newTodo);
    }
  })
  .catch(function(err) {
    console.log(err);
  });
 
}

